my_app:
  gunicorn_bind: 127.0.0.1:8000
  dns_name: dev.my-app.com
  venv_dir: /home/vagrant/my_app_env
  work_dir: /vagrant