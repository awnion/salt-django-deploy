{% set my_app = pillar['my_app'] %}

my_app.venv:
  virtualenv.managed:
    - name: {{ my_app['venv_dir'] }}
    - system_site_packages: False
    - require:
      - pkg: python

my_app.pip:
  pip.installed:
    - bin_env: {{ my_app['venv_dir'] }}
    - names:
      - Django==1.6
      - gunicorn==18.0
    - require:
      - virtualenv: my_app.venv

my_app.nginx.conf:
  file.managed:
    - name: /etc/nginx/sites-enabled/my_app.conf
    - source: salt://my_app/nginx.my_app.conf
    - context: # помимо переменных вроде pillar, мы можем передать дополнительный контекст для тепмлейта
        bind: {{ my_app['gunicorn_bind'] }}
        dns_name: {{ my_app['dns_name'] }}
    - template: jinja
    - makedirs: True
    - watch_in:
      - service: nginx

my_app.supervisor.conf:
  file.managed:
    - name: /etc/supervisor/conf.d/my_app.conf
    - source: salt://my_app/supervisor.my_app.conf
    - context:
        app_name: my_app
        bind: {{ my_app['gunicorn_bind'] }}
        gunicorn: {{ my_app['venv_dir'] }}/bin/gunicorn
        directory: {{ my_app['work_dir'] }}
        workers: {{ grains['num_cpus'] * 2 + 1 }}  # в академических целях выпендрился
    - template: jinja
    - makedirs: True

my_app.supervisor:
  supervisord.running:
    - name: my_app
    - watch:
      - file: my_app.supervisor.conf
    - require:
      - pip: my_app.pip
      - pkg: supervisor
