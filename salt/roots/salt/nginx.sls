nginx:
  pkg:
    - installed
  service:
    - running
    - reload: True
    - require:
      - pkg: nginx
